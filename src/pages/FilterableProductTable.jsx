import React, { useState } from 'react'

import SearchBar from '../components/SearchBar'
import ProductTable from '../components/ProductTable'
export default props => {

    const [filterText, setFilterText] = useState('')
    const [inStockOnly, setInStockOnly] = useState(false)

    function handleFilterTextChange(filterText) {
        setFilterText(filterText)
    }
    
    function handleInStockChange(inStockOnly) {
        setInStockOnly(inStockOnly)
    }

    return (
        <div>
            <SearchBar
                filterText={filterText}
                inStockOnly={inStockOnly}
                onFilterTextChange={handleFilterTextChange}
                onInStockChange={handleInStockChange}
            />
            <ProductTable
                products={props.products}
                filterText={filterText}
                inStockOnly={inStockOnly}
            />
        </div>
    );
}
